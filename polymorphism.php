<?php
/**
 * Created by PhpStorm.
 * User: 20-11-15
 * Date: 22-Dec-15
 * Time: 4:19 AM
 */
class A{
    public function dp(){
        echo "Inside the base class";
    }
}
class B extends A{
    public function dp(){
        echo "Inside the child class";
    }
}
class C extends A{
//    public function dp(){
//        echo "Inside the C class";
//    }
}
$obj=new B();
$obj->dp();
$oc=new C();
$oc->dp();





#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#2
class Animal{
    public $name;
    public function __construct($name){
        $this->name=$name;
    }
}
class Dog extends Animal{
    function speak(){
        return "woof,woof";
    }
}
class Cat extends Animal{
    function speak(){
        return " Meow...";
    }
}
$animals=[new Dog('BoolDog'),new Cat('Tom')];
foreach($animals as $animal){
    echo $animal->name." says :".$animal->speak().'<br>';
}